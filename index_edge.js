/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0', '0', '1080', '540', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-1',
                                type: 'image',
                                rect: ['0px', '0px', '1080px', '540px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-1.png",'0px','0px']
                            },
                            {
                                id: 'nuves',
                                type: 'image',
                                rect: ['260px', '42px', '537px', '470px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"nuves.png",'0px','0px']
                            },
                            {
                                id: 'edificios',
                                type: 'image',
                                rect: ['0px', '0px', '1080px', '540px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"edificios.png",'0px','0px']
                            },
                            {
                                id: 'rayo-1',
                                type: 'image',
                                rect: ['9px', '463px', '427px', '53px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"rayo-1.png",'0px','0px'],
                                userClass: "rayos"
                            },
                            {
                                id: 'rayo-2',
                                type: 'image',
                                rect: ['641px', '463', '409px', '51px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"rayo-2.png",'0px','0px'],
                                userClass: "rayos"
                            },
                            {
                                id: 'isla-gruup',
                                type: 'group',
                                rect: ['249', '160', '549', '370', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'pluma-2',
                                    type: 'image',
                                    rect: ['427px', '81px', '122px', '118px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"pluma-2.png",'0px','0px'],
                                    userClass: "plumas"
                                },
                                {
                                    id: 'pluma-1',
                                    type: 'image',
                                    rect: ['0px', '69px', '122px', '118px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"pluma-1.png",'0px','0px'],
                                    userClass: "plumas"
                                },
                                {
                                    id: 'isla',
                                    type: 'image',
                                    rect: ['75px', '140px', '410px', '230px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"isla.png",'0px','0px']
                                },
                                {
                                    id: 'texto',
                                    type: 'image',
                                    rect: ['134px', '233px', '293px', '56px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"texto.png",'0px','0px']
                                },
                                {
                                    id: 'pc',
                                    type: 'image',
                                    rect: ['196px', '0px', '253px', '193px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"pc.png",'0px','0px']
                                },
                                {
                                    id: 'seguro',
                                    type: 'image',
                                    rect: ['77px', '42px', '126px', '151px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"seguro.png",'0px','0px']
                                },
                                {
                                    id: 'avatar-3',
                                    type: 'image',
                                    rect: ['49px', '154px', '68px', '101px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"avatar-3.png",'0px','0px']
                                },
                                {
                                    id: 'avatar-4',
                                    type: 'image',
                                    rect: ['311px', '69px', '141px', '135px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"avatar-4.png",'0px','0px']
                                },
                                {
                                    id: 'avatar-2',
                                    type: 'image',
                                    rect: ['143px', '0px', '71px', '102px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"avatar-2.png",'0px','0px']
                                },
                                {
                                    id: 'avatar-1',
                                    type: 'image',
                                    rect: ['229px', '126px', '62px', '78px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"avatar-1.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'sol',
                                type: 'image',
                                rect: ['735px', '16px', '106px', '106px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"sol.png",'0px','0px']
                            },
                            {
                                id: 'titulo-1',
                                type: 'image',
                                rect: ['496px', '476px', '584px', '64px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"titulo-1.png",'0px','0px']
                            },
                            {
                                id: 'titulo-2',
                                type: 'image',
                                rect: ['0px', '11px', '529px', '89px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"titulo-2.png",'0px','0px']
                            },
                            {
                                id: 'n-1',
                                type: 'image',
                                rect: ['227px', '415px', '64px', '65px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-1.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-2',
                                type: 'image',
                                rect: ['106px', '383px', '64px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-2.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-3',
                                type: 'image',
                                rect: ['54px', '314px', '64px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-3.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-4',
                                type: 'image',
                                rect: ['145px', '253px', '65px', '65px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-4.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-5',
                                type: 'image',
                                rect: ['73px', '164px', '65px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-5.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-6',
                                type: 'image',
                                rect: ['184px', '146px', '64px', '65px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-6.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-7',
                                type: 'image',
                                rect: ['291px', '100px', '64px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-7.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-8',
                                type: 'image',
                                rect: ['538px', '69px', '65px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-8.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-9',
                                type: 'image',
                                rect: ['672px', '90px', '64px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-9.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-10',
                                type: 'image',
                                rect: ['756px', '137px', '64px', '65px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-10.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-11',
                                type: 'image',
                                rect: ['910px', '132px', '64px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-11.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-12',
                                type: 'image',
                                rect: ['863px', '225px', '65px', '65px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-12.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-13',
                                type: 'image',
                                rect: ['964px', '270px', '65px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-13.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-14',
                                type: 'image',
                                rect: ['900px', '346px', '64px', '64px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-14.png",'0px','0px'],
                                userClass: "n"
                            },
                            {
                                id: 'n-15',
                                type: 'image',
                                rect: ['814px', '378px', '64px', '65px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"n-15.png",'0px','0px'],
                                userClass: "n"
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0', '0', '1080', '540', 'auto', 'auto'],
                            c: [
                            {
                                id: 'back',
                                type: 'rect',
                                rect: ['0px', '0px', '1080px', '540px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'Group-box',
                                type: 'group',
                                rect: ['244', '220', '531', '164', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box',
                                    type: 'image',
                                    rect: ['0px', '0px', '531px', '164px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"box.png",'0px','0px']
                                },
                                {
                                    id: 'guion',
                                    type: 'image',
                                    rect: ['167px', '130px', '196px', '5px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"guion.png",'0px','0px']
                                },
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['16px', '16px', '494px', '108px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"],
                                    textStyle: ["", "", "20px", "", ""]
                                }]
                            },
                            {
                                id: 'btn-close',
                                type: 'image',
                                rect: ['745px', '194px', '51px', '51px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn-close.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1080px', '540px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1080px', '540px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['490px', '220px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1080px', '540px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
