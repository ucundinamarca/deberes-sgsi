var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                t.animation();
                t.events();
                stage1.play();
            });
        },
        methods: {
            events: function () {
                var t = this;
                ivo(ST + "btn-close").on("click", function () {
                    ivo.play( "clic");
                    stage2.timeScale(3).reverse();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(".n").on("click", function () {
                    let n = parseInt((ivo(this).attr("id").split("-")[1]))-1;
                    ivo(ST + "Text").text(textos[n].texto);
                    ivo(this).removeClass("animated").removeClass("infinite").removeClass("tada");
                    stage2.timeScale(3).play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
            },
            animation: function () {
                stage1 = new TimelineMax({
                    
                });
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "titulo-2", .8, {x: -1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".rayos", .4, {x: 600, opacity: 0, scaleX: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.append(TweenMax.from(ST + "titulo-1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "isla-gruup", .8, {y: -1300, opacity: 0,onComplete: function () {
                    isla.play();
                }}), 0);
                stage1.append(TweenMax.staggerFrom(".n", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.append(TweenMax.staggerFrom(".plumas", .4, {scale: 0, ease: Elastic.easeOut.config(0.3, 0.4),onStart: function () {
                    ivo(".n").addClass("animated").addClass("infinite").addClass("tada");
                    
                }}, .2), 0);
                
             
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "guion", .8, {y: -50, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "Text", .3, { opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "btn-close", .8, {scale:0,rotation:900, opacity: 0}), 0);
                stage2.stop();

                sol = new TimelineMax({repeat:-1});
                sol.append(TweenMax.to(ST + "sol", 3, { opacity: .3}), 0);
                sol.append(TweenMax.to(ST + "sol", 3, { opacity: 1}), 0);

                isla = new TimelineMax({repeat:-1});
                isla.append(TweenMax.to(ST + "isla-gruup", 3, { y: 20}), 0);
                isla.append(TweenMax.to(ST + "isla-gruup", 3, { y: 0}), 0);
                isla.stop();
               

            }
        }
    });
}