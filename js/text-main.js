textos=[
    {
        texto:"Garantizar al titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data."
    },
    {
        texto:"Solicitar y conservar, copia de la respectiva autorización otorgada por el titular para el tratamiento de datos personales."
    },
    {
        texto:"Informar debidamente al titular sobre la finalidad de la recolección y los derechos que le asisten en virtud de la autorización otorgada."
    },
    {
        texto:"Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento."
    },
    {
        texto:"Garantizar que la información sea veraz, completa, exacta, actualizada, comprobable y comprensible."
    },
    {
        texto:"Actualizar oportunamente la información, atendiendo de esta forma todas las novedades respecto de los datos del titular. Adicionalmente, se deberán implementar todas las medidas necesarias para que la información se mantenga actualizada."
    },
    {
        texto:"Rectificar la información cuando sea incorrecta y comunicar lo pertinente."
    },
    {
        texto:"Tramitar las consultas y reclamos formulados en los términos señalados por la ley."
    },
    {
        texto:"Identificar cuando determinada información se encuentra en discusión por parte del titular."
    },
    {
        texto:"Informar a solicitud del titular sobre el uso dado a sus datos."
    },
    {
        texto:"Abstenerse de circular información que esté siendo controvertida por el titular y cuyo bloqueo haya sido ordenado por la Superintendencia de Industria y Comercio."
    },
    {
        texto:"Cumplir los requerimientos e instrucciones que imparta la Superintendencia de Industria y Comercio sobre el tema en particular."
    },
    {
        texto:"Velar por el uso adecuado de los datos personales de los menores, en aquellos casos en que se entra autorizado el tratamiento de sus datos."
    },
    {
        texto:"Permitir el acceso a la información únicamente a las personas que pueden tener acceso a ella."
    },
    {
        texto:"Usar los datos personales del titular sólo para aquellas finalidades para las que se encuentre facultada debidamente y respetando en todo caso la normatividad vigente sobre protección de datos personales."
    }


]